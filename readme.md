# Spice Research Test

## Installation
```bash
$ git clone project_repository
```

## For Running Test in Localhost

```bash
$ php -S localhost:8000
```
This creates a localhost with port 8000.

## For Running Test in Docker
- Install docker, docker-compose
- Stop all httpd or apache services
- Create folder /var/www/html if doesnot exist

```bash
$ sudo docker-compose up -d
$ sudo docker-compose up
```

- You can access project in localhost:8080

## Useful docker commands

- Stop
```bash
$ sudo docker-compose stop
```
- Kill Docker Processes
```bash
$ sudo docker-compose kill
```
- Remove Docker Container
```bash
$ sudo docker-compose rm -f
```

- Build Dockerfile
```bash
$ sudo docker-compose up -d
```
