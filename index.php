<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Spice Research Test</title>
        <!-- css for this test -->
        <link rel="stylesheet" type="text/css" href="/public/css/style.css">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Spice Research Test
                </div>
                <!-- Links to test and index page -->
                <?php include('views/includes/links.php') ?>
            </div>
        </div>
    </body>
</html>