<!DOCTYPE html>
<html>
<head>
    <title>Spice Research Test</title>
    <link rel="stylesheet" type="text/css" href="/public/css/style.css">
</head>
<body>
    <?php include('includes/links.php') ?>
    <form method="post" action="#">
        <center>
            <div class="card centered">
                <input type="text" name="letters" value="<?php echo isset($_POST['submit_test_one']) ? $_POST['letters'] : '' ?>">
                <button type="submit" name="submit_test_one">Submit</button>
            </div>
        </center>
    </form>
    <center>
        <h1>Output</h1>
        <div class="card centered">
            <?php
                if(isset($_POST['submit_test_one'])) { // check if form is submitted 
                    $input = $_POST['letters']; // get input
                    echo "Position : ".find_letter_a($input, $position = 0); // result
                }

                // find position function
                function find_letter_a($letters, $position)
                {
                    while (isset($letters[$position])) {
                        if ($letters[$position] == 'a') {
                            return $position;
                        }
                        // recursion method for finding a in string
                        find_letter_a($letters, ++$position);
                    }
                    return "N/A"; // if no position found
                }
            ?>
        </div>
    </center>
</body>
</html>