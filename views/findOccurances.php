<!DOCTYPE html>
<html>
<head>
    <title>Spice Research Test</title>
    <link rel="stylesheet" type="text/css" href="/public/css/style.css">
</head>
<body>
    <?php include('includes/links.php') ?>
    <form method="post" action="#">
        <center>
            <div class="card centered">
                <input type="text" name="letters" value="<?php echo isset($_POST['submit_test_one']) ? $_POST['letters'] : '' ?>">
                <button type="submit" name="submit_test_one">Submit</button>
            </div>
        </center>
    </form>
    <center>
        <h1>Output</h1>
        <div class="card centered">
            <?php
                if(isset($_POST['submit_test_one'])) { // check if form is submitted 
                    $letters = $_POST['letters']; // get input
                    $num = 0;
                    $string_occurance = [];
                    while (isset($letters[$num])) {
                        $count = 0;
                        if (!isset($string_occurance[$letters[$num]]) && $letters[$num] != ' ') {
                            $string_occurance[$letters[$num]] = find_occurance($letters, $letters[$num], $position = 0, $count);
                        }
                        $num++;
                    }
                    $total_characters = 0;
                    foreach ($string_occurance as $key => $occurance) {
                        echo $key." : ".$occurance."<br>";
                        $total_characters +=$occurance;
                    }
                    echo "Total Characters = ".$total_characters;
                }

                // find if letter exists
                function find_occurance($letters, $letter, $position, $count)
                {
                    while (isset($letters[$position])) {
                        // check if position found
                        if ($letters[$position] == $letter) {
                            $count++;
                        }
                        //recursion on string
                        find_occurance($letters, $letter, ++$position, $count);
                    }
                    return $count;
                }
            ?>
        </div>
    </center>
</body>
</html>