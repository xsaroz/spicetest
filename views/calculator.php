<!DOCTYPE html>
<html>
<head>
    <title>Spice Research Test</title>
    <link rel="stylesheet" type="text/css" href="/public/css/style.css">
</head>
<body>
    <?php include('includes/links.php') ?>
    <form method="post" action="#">
        <center>
            <div class="card centered">
                <input type="text" name="letters" value="<?php echo isset($_POST['submit_test_one']) ? $_POST['letters'] : '' ?>">
                <button type="submit" name="submit_test_one">Submit</button>
            </div>
        </center>
    </form>
    <center>
        <h1>Output</h1>
        <div class="card centered">
            <?php
            $position = 0;
                if(isset($_POST['submit_test_one'])) { // check if form is submitted
                    $letters = $_POST['letters'];
                    $letters = categorize_string($letters);
                }

                // categorize strings to operator or number and save in array
                function categorize_string($letters) {
                    $position = 0;
                    $index = 0;
                    $data = [];
                    while (isset($letters[$position])) {
                        $result = get_letter_type($letters, $position);
                        if ($result['type'] == 'number' && $data[$index]['type'] == 'number' && isset($data[$index])) {
                            $data[$index]['value'] .= $result['value'];
                        } else {
                            $index += 1;
                            $data[$index] = $result;
                        }
                        $position += 1;
                    }
                    // Validate if calculation can be made
                    $validate = validation($data);
                    if ($validate['status']) {
                        // make division operations first
                        $divide = divide($data);
                        //make multiplication operations second
                        $multiply = multiply($divide);
                        // make addition operations third
                        $add = add($multiply);
                        // make substraction operations fourth
                        $subtract = subtract($add);
                        // show value
                        echo $subtract[1]['value'];
                    } else {
                        // validation message with error class
                        echo "<span class='error'>".$validate['message']."</span>";
                    }
                }

                // Validation of all strings
                function validation($data) {
                    // validation default message
                    $validate = [
                        'status' => true,
                        'message' => 'Validation Passed' 
                    ];
                    foreach ($data as $key => $value) {
                        // operand validation
                        if (isset($data[$key + 1])) {
                            // check validation conditions
                            if ($value['type'] == 'operator' && $data[$key + 1]['type'] == 'operator' && $value['value'] == $data[$key + 1]['value']) {
                                $validate['status'] = false;
                                $validate['message'] = 'Operator Repeated.';
                            }
                            if ($value['type'] == 'operator' && $value['value'] == '/' && $data[$key + 1]['value'] == '0') {
                                $validate['status'] = false;
                                $validate['message'] = 'Divisible by Zero.';
                            }
                        }
                    }
                    return $validate;
                }

                // return data catagorizing them into different operator and number
                function get_letter_type($letters, $position, $number = '0') {
                    $letter = $letters[$position];
                    if ($letter == '/' || $letter == '*' || $letter == '+' || $letter == '-') {
                        return ['type' => 'operator', 'value' => $letter];
                    }
                    return ['type' => 'number', 'value' => $letter];
                }

                // after a single calculations change the index values
                function change_array_values($data, $key, $number, $new_data = []) {
                    $new_index = 1;
                    foreach ($data as $index => $value) {
                        if ($key == $index) {
                            $new_index -= 1;
                            $new_data[$new_index] = ['type' => 'number', 'value' => $number];
                            $new_index +=1;
                        } else {
                            if (($key + 1) != $index) {
                                $new_data[$new_index] = $value;
                                $new_index += 1;
                            }
                        }
                    }
                    return $new_data;
                }
                
                // To calculate all divisions
                function divide($data) {
                    foreach ($data as $key => $value) {
                        if ($value['value'] == '/') {
                            $prev_number = $data[$key - 1]['value'];
                            $next_number = $data[$key + 1]['value'];
                            $division_value = floatval($prev_number) / floatval($next_number);
                            $data = change_array_values($data, $key, $division_value);
                            divide($data);
                        }
                    }
                    return $data;
                }

                // To calculate all multiplications
                function multiply($data) {
                    foreach ($data as $key => $value) {
                        if ($value['value'] == '*') {
                            $prev_number = $data[$key - 1]['value'];
                            $next_number = $data[$key + 1]['value'];
                            $multiplication_value = floatval($prev_number) * floatval($next_number);
                            $data = change_array_values($data, $key, $multiplication_value);
                            multiply($data);
                        }
                    }
                    return $data;
                }

                // To calculate all additions
                function add($data) {
                    foreach ($data as $key => $value) {
                        if ($value['value'] == '+') {
                            $prev_number = $data[$key - 1]['value'];
                            $next_number = $data[$key + 1]['value'];
                            $addition_value = floatval($prev_number) + floatval($next_number);
                            $data = change_array_values($data, $key, $addition_value);
                            add($data);
                        }
                    }
                    return $data;
                }

                // To calculate all subtractions
                function subtract($data) {
                    foreach ($data as $key => $value) {
                        if ($value['value'] == '-' && isset($data[$key+1])) {
                            $prev_number = $data[$key - 1]['value'];
                            $next_number = $data[$key + 1]['value'];
                            $subtract_value = floatval($prev_number) - floatval($next_number);
                            $data = change_array_values($data, $key, $subtract_value);
                            subtract($data);
                        }
                    }
                    return $data;
                }
                ?>
            </div>
        </center>
    </body>
    </html>